class CreateExpenses < ActiveRecord::Migration[5.1]
  def change
    create_table :expenses do |t|
      t.references :user, foreign_key: true
      t.decimal :amout
      t.text :tags

      t.timestamps
    end
  end
end
